RedditClone::Application.routes.draw do

  resources :users
  resource :session
  resources :subs
  resources :comments
  resources :links do
    post "upvote"
    post "downvote"
  end

end
