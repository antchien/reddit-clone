module SessionsHelper

  def current_user
    return nil if session[:session_token].nil?
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def log_in_user(user)
    user.reset_session_token!
    session[:session_token] = user.session_token
    user.save!
    @current_user = user
  end

  def logged_in?
    !!current_user
  end

end
