class Link < ActiveRecord::Base
  attr_accessible :title, :url, :description, :user_id

  has_many(
  :linksubs,
  class_name: "LinkSub",
  foreign_key: :link_id,
  primary_key: :id,
  inverse_of: :link
  )

  has_many :subs, through: :linksubs, source: :sub

  has_many(
  :comments,
  class_name: "Comment",
  foreign_key: :link_id,
  primary_key: :id,
  inverse_of: :link
  )

  def comments_by_parent_id
    comment_hash = {}
    comments.each do |comment|
      comment_hash[comment.id] = comment.children unless comment.children.empty?
    end
    comment_hash[nil] = comments.where("parent_id IS NULL")
    comment_hash
  end

end
