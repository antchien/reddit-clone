class Sub < ActiveRecord::Base
  attr_accessible :user_id, :name
  validates :user_id, :name, presence: true
  validates :name, uniqueness: true


  belongs_to(
  :moderator,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

  has_many(
  :linksubs,
  class_name: "LinkSub",
  foreign_key: :sub_id,
  primary_key: :id
  )

  has_many :links, through: :linksubs, source: :link

end
