class Comment < ActiveRecord::Base
  attr_accessible :link_id, :parent_id, :user_id, :body

  belongs_to(
  :link,
  class_name: "Link",
  foreign_key: :link_id,
  primary_key: :id
  )

  belongs_to(
  :parent,
  class_name: "Comment",
  foreign_key: :parent_id,
  primary_key: :id
  )

  belongs_to(
  :author,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

  has_many(
  :children,
  class_name: "Comment",
  foreign_key: :parent_id,
  primary_key: :id
  )
end
