# require 'debugger'
class User < ActiveRecord::Base
  attr_accessible :email, :password, :session_token
  attr_reader :password
  validates :password, length: {minimum: 6, allow_nil: true}
  #validates :password_digest, :email, presence: true

  has_many(
  :subs_authored,
  class_name: "Sub",
  foreign_key: :user_id,
  primary_key: :id
  )

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)
    if !!user && user.has_password?(password)
      return user
    end

    nil
  end

  def reset_session_token!
    self.session_token = SecureRandom.urlsafe_base64(16)
  end

  def password=(secret)
    #p self
    @password = secret
    self.password_digest = BCrypt::Password.create(secret)
  end

  def has_password?(password)
    #p self
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end


end
