class LinksController < ApplicationController
  before_filter :user_logged_in?

  def new
    @subs = Sub.all
  end

  def upvote
    uv = UserVote.new(link_id: params[:link_id], user_id: current_user.id, vote: 1)
    uv.save!
    redirect_to link_url(params[:link_id])
  end

  def downvote
    uv = UserVote.new(link_id: params[:link_id], user_id: current_user.id, vote: -1)
    uv.save!
    redirect_to link_url(params[:link_id])
  end

  def show
    @link = Link.find(params[:id])
    @comments = @link.comments
    @comments_by_parent_id = @link.comments_by_parent_id
  end

  def create
    user = current_user
    link = Link.new(params[:link])
    link.update_attribute(:user_id, user.id)
    params[:comment][:user_id] = user.id
    link.comments.new(params[:comment])
    link.linksubs.new(sub_id: params[:sub_id])
    if link.save
      redirect_to link
    else
      flash[:error] = link.errors.full_messages
      redirect_to new_link_url
    end

    #create link + save
    #link to sub via link_subs
    #link to comment if they submit one
  end

  def user_logged_in?
    unless logged_in?
      redirect_to new_session_url
    end
  end

end
