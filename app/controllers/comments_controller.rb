class CommentsController < ApplicationController

  def show
    render :show
  end

  def create
    comment = Comment.new(params[:comment])
    comment.update_attribute(:user_id, current_user.id)
    if comment.save
      redirect_to comment

    else
      flash[:error] = comment.errors.full_messages
      redirect_to new_comment_url
    end
  end
end
