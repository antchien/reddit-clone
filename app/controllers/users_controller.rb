class UsersController < ApplicationController

  def create
    user = User.new(params[:user])
    if user.save
      log_in_user(user)
      redirect_to subs_url
    else
      flash[:error] = user.errors.full_messages
      redirect_to new_user_url
    end
  end

end
