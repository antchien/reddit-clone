require 'debugger'

class SessionsController < ApplicationController

  def create
    user = User.find_by_credentials(params[:user][:email], params[:user][:password])
    if !!user
      log_in_user(user)
      redirect_to links_url
    else
      flash[:error] = "Invalid credentials"
      redirect_to new_session_url
    end
  end

  def destroy
    user = current_user
    user.reset_session_token!
    session[:session_token] = nil
    user.save!
    redirect_to new_session_url
  end


end
