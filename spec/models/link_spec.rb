require 'spec_helper'

describe Link do
 #pending "add some examples to (or delete) #{__FILE__}"

 let! (:sport_sub) {FactoryGirl.create(:sub)}
 let! (:first_user) {FactoryGirl.create(:user)}
 let! (:link_1) {FactoryGirl.create(:link)}
 let! (:linksub_1) {FactoryGirl.create(:link_sub)}

 it "it is connected to its subs" do
   expect(link_1.subs.first).to be_instance_of(Sub)
 end

 describe "#comments_by_parent_id" do
   let! (:comment_1) {FactoryGirl.create(:comment)}
   let! (:comment_1_1) {FactoryGirl.create(:comment, parent_id: 1)}
   let! (:comment_1_2) {FactoryGirl.create(:comment, parent_id: 1)}
   let! (:comment_2) {FactoryGirl.create(:comment)}
   let! (:comment_2_1) {FactoryGirl.create(:comment, parent_id: 4)}

   it "should return the correct comments hash" do
     answer = {comment_1.id => [comment_1_1, comment_1_2], comment_2.id => [comment_2_1], nil => [comment_1, comment_2]}
     expect(link_1.comments_by_parent_id).to eq(answer)
   end

   it "" do

   end

 end



end
