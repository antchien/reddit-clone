require 'spec_helper'

describe Sub do
  #pending "add some examples to (or delete) #{__FILE__}"
  let! (:sport_sub) {FactoryGirl.create(:sub)}
  let! (:first_user) {FactoryGirl.create(:user)}

  # let! (:sport_sub_no_name) {FactoryGirl.create(:sub, name: nil)}


  it "it stores an owner" do
    # debugger
    expect(sport_sub.moderator.id).to eq(1)
  end

  it "fails if name is empty" do
    expect(FactoryGirl.build(:sub, name: nil)).not_to be_valid
  end

  it "fails if moderator is empty" do
    expect(FactoryGirl.build(:sub, user_id: nil)).not_to be_valid
  end

  it "fails if subname is already taken" do
    expect(FactoryGirl.build(:sub)).not_to be_valid
  end

  describe "link associations" do

    let! (:link_1) {FactoryGirl.create(:link)}
    let! (:linksub_1) {FactoryGirl.create(:link_sub)}

    it "returns all links" do
      sport_sub.links.first.should be_instance_of(Link)
    end

  end


end
