require 'spec_helper'
require 'debugger'

describe User do

  let!(:first_user) {FactoryGirl.create(:user)}
  let!(:db_user){User.find(first_user.id)}

  it "should not store the password" do
    db_user.password.should be_nil
  end

  it "should store password digest" do
    expect(db_user.password_digest).not_to be_nil
  end

  describe "::find_by_credentials" do
    it "return nil for bad user credentials" do
      expect(User.find_by_credentials("asdf", "n")).to be_nil
    end

    it "return user object for good user credentials" do
      a = User.find_by_credentials("hi@example.com", "hello1")

      expect(a.email).to eq(first_user.email)
    end

  end

end
