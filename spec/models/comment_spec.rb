require 'spec_helper'

describe Comment do
  #pending "add some examples to (or delete) #{__FILE__}"

  let! (:sport_sub) {FactoryGirl.create(:sub)}
  let! (:first_user) {FactoryGirl.create(:user)}
  let! (:link_1) {FactoryGirl.create(:link)}
  let! (:linksub_1) {FactoryGirl.create(:link_sub)}
  let! (:comment_1) {FactoryGirl.create(:comment)}
  let! (:comment_1_1) {FactoryGirl.create(:comment, parent_id: 1)}

  it "should have appropriate link" do
    expect(comment_1.link).to be_instance_of(Link)
  end

  it "should have appropriate author" do
    expect(comment_1.author).to be_instance_of(User)
  end

  it "should have appropriate parent" do
    expect(comment_1_1.parent).to be_instance_of(Comment)
  end

  it "should have many children" do
    expect(comment_1.children.first).to be_instance_of(Comment)
  end


end
