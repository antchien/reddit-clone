# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    user_id 1
    link_id 1
    parent_id nil
    body "THAT IS OUTRAGEOUS"
  end
end
