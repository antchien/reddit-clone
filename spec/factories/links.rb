# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :link do
    title "google"
    url "www.google.com"
    description "a cool site"
    user_id 1
  end
end
